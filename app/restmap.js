import Rest from './rest';

export default class RestMap extends Rest {

    constructor(url) {
        super(url);
    }

    async getAll(action) {
        await super.getAll(
            data => action(RestMap.toMap(data))
        );
    }

    static toMap(json) {
        return new Map(
            json.map(
                item => [item._id, item]
            )
        )
    }

    static toString(map) {
        return [...map.keys()].reduce(
            (result, key) => result + key + ": " + JSON.stringify(map.get(key)) + "<br/>",
            ""
        );
    }
}