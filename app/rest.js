export default class Rest {

    constructor(url) {
        this.url = url;
    }

    static check(method, response) {
        if (response.status != 200) {
            throw new Error(method + " " + response.url + " " + response.status + " (" + response.statusText + ")");
        }
    }

    async getAll(action) {
        const method = "GET";
        let data;
        try {
            const response = await fetch(this.url);
            Rest.check(method, response);
            data = await response.json();
            console.log("getAll: ", data);
        }
        catch (error) {
            console.log(error);
            data = null;
        }
        action(data);
    }
    
    async getOne(id, action) {
        const method = "GET";
        let data;
        try {
            const response = await fetch(this.url + "/" + id);
            Rest.check(method, response);
            data = await response.json();
        }
        catch (error) {
            console.log(error);
            data = null;
        }
        action(data);
    }

    async postOne(data, action) {
        const method = "POST";
        try {
            const response = await fetch(this.url, {
                method: method,
                body: JSON.stringify(data)
            });
            Rest.check(method, response);
            data = await response.json();
        }
        catch (error) {
            console.log(error);
            data = null;
        }
        action(data);
    }

    async putOne(id, data, action) {
        const method = "PUT";
        try {
            const response = await fetch(this.url + "/" + id, {
                method: method,
                body: JSON.stringify(data)
            });
            Rest.check(method, response);
            data = await response.json();
        }
        catch (error) {
            console.log(error);
            data = null;
        }
        action(data);
    }

    async deleteOne(id, action) {
        const method = "DELETE";
        try {
            const response = await fetch(this.url + "/" + id, {
                method: method
            });
            Rest.check(method, response);
        }
        catch (error) {
            console.log(error);
            id = -1;
        }
        action(id);
    }
}