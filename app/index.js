import User from "./user";
import RestMap from './restmap';

const users = new RestMap("users.json");

const userData = new User(
    1,
    "Joe"
);

users.getAll(
    data => document.getElementById("data").innerHTML = RestMap.toString(data)
);

users.getOne(
    1,
    data => console.log("GET (one)", data)
);

users.postOne(
    userData,
    data => console.log("POST", data)
);

users.putOne(
    userData._id,
    userData,
    data => console.log("PUT", data)
);

users.deleteOne(
    1,
    id => console.log("DELETE", id)
);
