export default class User {

    constructor(id, name) {
        this.setId(id);
        this.setName(name);
    }

    setId(id) {
        if (false) {
            throw new Error("User: id not valid " + id);
        }
        this._id = id;
    }

    setName(name) {
        if (false) {
            throw new Error("User: name not valid " + name);
        }
        this._name = name;
    }
    
    toString() {
        return "Id: " + this._id + ", Name: " + this._name;
    }
}